from django.urls import path

from .views import Homepage, Info

urlpatterns = [
    path('', Homepage.as_view(), name='homepage'),
    path('info/', Info.as_view(), name='info')
]
