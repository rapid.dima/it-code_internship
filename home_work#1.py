def month_int_to_string(day: int, month: int, year: int):
    """Задание №1"""
    month_list = {
        1: 'января',
        2: 'февраля',
        3: 'марта',
        4: 'апреля',
        5: 'мая',
        6: 'июня',
        7: 'июля',
        8: 'августа',
        9: 'сентября',
        10: 'октября',
        11: 'ноября',
        12: 'декабря',
    }
    return f'{day} {month_list[month]} {year} года'


def counter_name(names: tuple):
    """Задание №2"""
    count = 0
    numbers_names = {}
    for name in names:
        if name in numbers_names.keys():
            numbers_names[name] += 1
            continue
        else:
            numbers_names[name] = 1
    return numbers_names


def print_person(person: dict):
    '''Задания №3'''
    person_keys = person.keys()
    if 'first_name' and 'last_name' not in person_keys:
        return 'Нет данных'
    elif 'first_name' not in person_keys:
        return f'{person["last_name"]} {person["middle_name"]}'
    elif 'last_name' not in person_keys:
        return f'{person["first_name"]}'
    elif 'middle_name' not in person_keys:
        return f'{person["last_name"]} {person["first_name"]}'


def natural_numbers(number: int):
    '''Задания №4'''
    if number < 2:
        return False
    for i in range(2, int(number ** 0.5) + 1):
        if number % i == 0:
            return False
    return True


def integer_sort(*args):
    '''Задания №5'''
    inter_list = []
    for val in args:
        if isinstance(val, int) and val not in inter_list:
            inter_list.append(val)
    inter_list.sort()
    return inter_list

